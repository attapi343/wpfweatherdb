﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LiteDB;



namespace WpfAppWeather
{
    public partial class MainWindow : Window
    {
        Guid CurrentRecord;
        string SelectCity = "";
        string nameCity = "";
        Dictionary<string, string> dicCity = new Dictionary<string, string>()
        {
            {"Санкт-петербург" ,"saint-petersburg"},
            {"Волгоград","volgograd"},
            {"Москва","moscow"},
            {"Сыктывкар","syktyvkar"},
            {"Казань","kazan"}
        };

        public MainWindow()
        {
            InitializeComponent();
            UpdateData.IsEnabled = false;
            SelectItemTable(0);

            
            WeatherCity.DataContext = dicCity.Keys.ToList();  //Заполняю ComboBox 

        }

        //выбор элемента таблицы
        private void SelectItemTable(int val)
        {
            grid.SelectedIndex = val;
            grid.Focus();
            
        }

        //Загрузка данных для таблицы
        private void grid_Loaded(object sender, RoutedEventArgs e)
        {
            using (var db = new LiteDatabase(@"WeatherData.db"))
            {
                var collection = db.GetCollection<TableWeather>("weathers");
                var col = collection.FindAll().ToList<TableWeather>();

                List<MyTable> result = new List<MyTable>(col.Count());

                for (int i=0;i < col.Count(); i++)
                {
                    result.Add(new MyTable(col[i].TabDateRecording.Date.ToString("dd.MM.yyyy"), col[i].TabTimeRecording, col[i].TabCity, col[i].TabTemperature, col[i].Id));
                }
 
                grid.ItemsSource = result;
       
            }
        }

        //Столбец таблицы невидим
        private void OnAutoGeneratingColumn(object sender, DataGridAutoGeneratingColumnEventArgs e)
        {
           
            PropertyDescriptor propertyDescriptor = (PropertyDescriptor)e.PropertyDescriptor;
            if (propertyDescriptor.DisplayName == "Id")
            {
                e.Column.Header = propertyDescriptor.DisplayName;
                e.Cancel = true;
            }
            else if (propertyDescriptor.DisplayName == "TabDateRecording")
                e.Column.Header = "Дата";
            else if (propertyDescriptor.DisplayName == "TabTimeRecording")
                e.Column.Header = "Время";
            else if (propertyDescriptor.DisplayName == "TabCity")
                e.Column.Header = "Город";
            else if (propertyDescriptor.DisplayName == "TabTemperature")
                e.Column.Header = "Температура";

        }

        //Обновление данных, после изменения
        public void DataUpdate(TableWeather rezdata)
        {
            using (var db = new LiteDatabase(@"WeatherData.db"))
            {
                var collection = db.GetCollection<TableWeather>("weathers");
                collection.Update(rezdata);
                CurrentRecord = rezdata.Id;
                ViewWidget(rezdata);
            }
            grid_Loaded(null, null);
        }

        //Заполнение виджетов из бд
        private void ViewWidget(TableWeather element)
        {           
            City.Content = "Погода в "+element.TabCity;
            Wind.Content = "Ветер "+element.TabWind +" м/с";
            Rainfall.Content = "Осадки "+element.TabRainfall.ToString()+" %";
            AtmosphericPressure.Content = "Давление "+element.TabAtmosphericPressure.ToString()+" мм. рт. ст.";
            DateRecording.Content = element.TabDateRecording.Date.ToString("dd.MM.yyyy");
            TimeRecording.Content = element.TabTimeRecording.ToString();
            Temperature.Content = "Температура "+element.TabTemperature.ToString()+"°";
            Weather.Content = "Погода "+element.TabWeather;
            SunriseSunset.Content ="Восход - "+element.TabSunrise.ToString()+"    Закат - "+element.TabSunset.ToString(); 
        }

        //создаем новую запись в бд
        private void AddDatabase(TableWeather recorddb)
        {
            // открывает базу данных, если ее нет - то создает
            using (var db = new LiteDatabase(@"WeatherData.db"))
            {
                var collection = db.GetCollection<TableWeather>("weathers");
                var NewRecord = new TableWeather
                {
                    TabCity = recorddb.TabCity,
                    TabDateRecording = recorddb.TabDateRecording,
                    TabTemperature = recorddb.TabTemperature,
                    TabWind = recorddb.TabWind,
                    TabRainfall = recorddb.TabRainfall,
                    TabAtmosphericPressure = recorddb.TabAtmosphericPressure,
                    TabTimeRecording = recorddb.TabTimeRecording,
                    TabWeather = recorddb.TabWeather,
                    TabSunrise = recorddb.TabSunrise,
                    TabSunset = recorddb.TabSunset
                };
                collection.Insert(NewRecord);                
            }
        }


        //Выбор города для парсинга
        private void ComboBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            nameCity = WeatherCity.SelectedValue.ToString();
            SelectCity = dicCity[nameCity];
            UpdateData.IsEnabled = true;

        }

        //Сбор данных о погоде
        private TableWeather WeatherDataCollection(string city)
        {
            string uri = "https://yandex.ru/pogoda/"+ city;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            StreamReader reader = new StreamReader(response.GetResponseStream(), Encoding.UTF8);
            string htmltext = reader.ReadToEnd();
            TableWeather datarec = new TableWeather();

            var rg = new Regex(@"<span class=\Dtemp__value\D>(.*?)</span>");
            datarec.TabTemperature = rg.Match(htmltext).Groups[1].Value;

            rg = new Regex(@"\Dterm__fact-icon\D></i><span class=\Dwind-speed\D>(.*?)</span>");

            if (rg.Match(htmltext).Groups[1].Value == "")
                datarec.TabWind = "0";
            else
                datarec.TabWind = rg.Match(htmltext).Groups[1].Value;

            rg = new Regex(@"icon_pressure-white term__fact-icon\D></i>(.*?)<span class=\Dfact__unit\D>");
            datarec.TabAtmosphericPressure = int.Parse(rg.Match(htmltext).Groups[1].Value);

            rg = new Regex(@"<i class=\Dicon icon_humidity-white term__fact-icon\D></i>(.*?)%</dd>");
            datarec.TabRainfall = int.Parse(rg.Match(htmltext).Groups[1].Value);

            rg = new Regex(@"Сейчас (.*?)</time>");
            datarec.TabTimeRecording = rg.Match(htmltext).Groups[1].Value;

            datarec.TabDateRecording = DateTime.Today;

            rg = new Regex(@"Восход</span><i class=\Dicon icon_sunrise icon_size_24\D data-width=\D24\D></i></dt><dd class=\Dsunrise-sunset__value\D>(.*?)</dd>");
            datarec.TabSunrise = rg.Match(htmltext).Groups[1].Value;

            rg = new Regex(@"i class=\Dicon icon_sunset icon_size_24\D data-width=\D24\D></i></dt><dd class=\Dsunrise-sunset__value\D>(.*?)</dd>");
            datarec.TabSunset = rg.Match(htmltext).Groups[1].Value;

            rg = new Regex(@"i-bem\D data-bem='{\Dday-anchor\D:{\Danchor\D:\w*}}\D>(.*?)</div>");
            datarec.TabWeather = rg.Match(htmltext).Groups[1].Value;

            return datarec;
        }

        private void DeleteEntry_Click(object sender, RoutedEventArgs e)
        {
            using (var db = new LiteDatabase(@"WeatherData.db"))
            {
                var collection = db.GetCollection<TableWeather>("weathers");
                collection.Delete(x => x.Id == CurrentRecord);
            }
            grid_Loaded(null, null);
            SelectItemTable(0);
        }

        //Кнопка парсировки сайта с погодой
        private void UpdateData_Click(object sender, RoutedEventArgs e)
        {
            TableWeather rez = WeatherDataCollection(SelectCity);
            rez.TabCity = nameCity;
            AddDatabase(rez);         
            ViewWidget(rez);
            CurrentRecord = rez.Id;
            grid_Loaded(null,null);
        }

        //переход в окно редактирования записи
        private void EditEntry_Click(object sender, RoutedEventArgs e)
        {
            EditEntryForms EditEntry = new EditEntryForms(this);

            using (var db = new LiteDatabase(@"WeatherData.db"))
            {
                var collection = db.GetCollection<TableWeather>("weathers");
                var col = collection.FindAll().ToList<TableWeather>();

                TableWeather r = col.Find(a => a.Id == CurrentRecord);
                EditEntry.ProcessingData(r);
 
            } 
        }

        private void Grid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            try
            {
                using (var db = new LiteDatabase(@"WeatherData.db"))
                {
                    var collection = db.GetCollection<TableWeather>("weathers");
                    var col = collection.FindAll().ToList<TableWeather>();

                    MyTable path = grid.SelectedItem as MyTable;
                    TableWeather r = col.Find(a => a.Id == path.Id);

                    CurrentRecord = r.Id;
                    ViewWidget(r);
                }
            }
            catch (Exception)
            {

            }

        }

    }
}
