﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WpfAppWeather
{
    public class TableWeather
    {
        public Guid Id { get; set; }
        public string TabCity { get; set; }
        public string TabTemperature { get; set; }
        public string TabWind { get; set; }
        public int TabRainfall { get; set; }
        public int TabAtmosphericPressure { get; set; }
        public DateTime TabDateRecording { get; set; }
        public string TabTimeRecording { get; set; }
        public string TabWeather { get; set; }
        public string TabSunrise { get; set; }
        public string TabSunset { get; set; }
    }

    class MyTable
    {
        public MyTable(string TabDateRecording, string TabTimeRecording, string TabCity, string TabTemperature, Guid Id)
        {
            this.TabDateRecording = TabDateRecording;
            this.TabTimeRecording = TabTimeRecording;
            this.TabCity = TabCity;
            this.TabTemperature = TabTemperature;
            this.Id = Id;
        }
        public Guid Id { get; set; }
        public string TabDateRecording { get; set; }
        public string TabTimeRecording { get; set; }
        public string TabCity { get; set; }
        public string TabTemperature { get; set; }

    }

}
