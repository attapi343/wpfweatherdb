﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace WpfAppWeather
{
    /// <summary>
    /// Interaction logic for EditEntryForms.xaml
    /// </summary>
    public partial class EditEntryForms : Window
    {

        TableWeather rezrecord = new TableWeather();

        MainWindow mainWindow;
        public EditEntryForms(MainWindow mainWindow)
        {
            this.mainWindow = mainWindow;
            InitializeComponent();
        }

        public  void ProcessingData(TableWeather newrecord)
        {
            Show();
            rezrecord = newrecord;
            City.Text = newrecord.TabCity;
            Wind.Text = newrecord.TabWind;
            Rainfall.Text = newrecord.TabRainfall.ToString();
            AtmosphericPressure.Text = newrecord.TabAtmosphericPressure.ToString();
            DateRecording.SelectedDate = newrecord.TabDateRecording;
            TimeRecording.Text = newrecord.TabTimeRecording.ToString();
            Temperature.Text = newrecord.TabTemperature;
            Weather.Text = newrecord.TabWeather;
            Sunrise.Text = newrecord.TabSunrise.ToString();
            Sunset.Text = newrecord.TabSunset.ToString();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //Сбор данных из виджетов
            rezrecord.TabCity = City.Text;
            rezrecord.TabWind = Wind.Text;
            rezrecord.TabRainfall = int.Parse(Rainfall.Text);
            rezrecord.TabAtmosphericPressure = int.Parse(AtmosphericPressure.Text);
            rezrecord.TabDateRecording = DateRecording.DisplayDate;
            rezrecord.TabTimeRecording = TimeRecording.Text;
            rezrecord.TabTemperature = Temperature.Text;
            rezrecord.TabWeather = Weather.Text;
            rezrecord.TabSunrise = Sunrise.Text;
            rezrecord.TabSunset = Sunset.Text;

            mainWindow.DataUpdate(rezrecord);
            Close();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Close();
        }
        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }

        private void TimeValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            //var tbText = ((TextBox)sender).Text; 
            //var vvedenniyText = e.Text; 
            Regex regex = new Regex("[^0-9:]+");
            e.Handled = regex.IsMatch(e.Text);
        }
        private void temperatureValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9+-]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}
